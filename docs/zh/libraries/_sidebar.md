- 传感器类
  - Adafruit传感器库
    - [Adafruit AHT10/20传感器库](/zh/libraries/sensors/adafruit/adafruit-ahtx0/adafruit-ahtx0.md)
    - [Adafruit SHT31传感器库](/zh/libraries/sensors/adafruit/adafruit-sht31/adafruit-sht31.md)
  - 其他传感器库
    - [电容性触摸传感库](/zh/libraries/sensors/others/capacitive-touch/capacitive-touch.md)

- 屏幕驱动类
  - Adafruit屏幕驱动库